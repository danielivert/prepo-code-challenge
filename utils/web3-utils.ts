import { BigNumber } from 'ethers'
import Web3 from 'web3'
import config from '../lib/config'

export const roundToDecimals = (
  value: number | string,
  decimals: number = config.ROUNDED_DECIMALS
): string => {
  const valueAsFloat = parseFloat(`${value}`)
  const roundedValue = valueAsFloat.toFixed(decimals)
  return valueAsFloat === 0 ? '0' : `${roundedValue}`
}

export const fromWeiToEther = (value: number | BigNumber): string => {
  const etherStringValue = Web3.utils.fromWei(`${value}`, 'ether')
  return roundToDecimals(etherStringValue)
}
