import {
  SupportedNetworks,
  UNISWAP_V2_CONTRACT_ADDRESS,
  UNISWAP_V2_DAI_ETH_ADDRESS,
} from './constants'

const config = {
  HOST: process.env.HOST ?? 'http://localhost:3000',
  NETWORK: (process.env.NEXT_PUBLIC_NETWORK as unknown as SupportedNetworks) ?? 'localhost',
  UNISWAP_V2_CONTRACT_ADDRESS,
  UNISWAP_V2_DAI_ETH_ADDRESS,
  ROUNDED_DECIMALS: 4,
}

const appConfig = {
  isProduction: !config.HOST.includes('localhost'),
}

export default { ...config, ...appConfig }
