import Navigation from '../components/Navigation'
import Swap from '../features/swap/Swap'

const Index: React.FC = () => {
  return (
    <>
      <Navigation />
      <Swap />
    </>
  )
}

export default Index
