/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ChakraProvider } from '@chakra-ui/react'
import { configure } from 'mobx'
import { AppProps } from 'next/app'

import { RootStoreProvider } from '../context/RootStoreProvider'
import theme from '../utils/theme'

import '@fontsource/inter/400.css'
import '@fontsource/nunito/700.css'

configure({ enforceActions: 'always' })

const App = ({ Component, pageProps }: AppProps): React.ReactElement => {
  return (
    <ChakraProvider theme={theme}>
      <RootStoreProvider>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <Component {...pageProps} />
      </RootStoreProvider>
    </ChakraProvider>
  )
}

export default App
