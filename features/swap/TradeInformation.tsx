import { Box, Heading, Text, useInterval } from '@chakra-ui/react'
import { observer } from 'mobx-react-lite'
import { useRootStore } from '../../context/RootStoreProvider'
import { ONE_SECOND } from '../../lib/constants'
import { roundToDecimals } from '../../utils/web3-utils'

const TradeInformation: React.FC = () => {
  const { uniswapStore } = useRootStore()

  useInterval(async () => {
    if (uniswapStore.daiInputValue && uniswapStore.ethInputValue) {
      await uniswapStore.getPriceForTradingPair()
    }
  }, ONE_SECOND * 5)

  return (
    <Box>
      <Heading as="h2" fontSize="sm">
        Current Rate
      </Heading>
      <Text fontSize="sm"> 1 ETH = {roundToDecimals(uniswapStore.tradingPairEth, 2)} DAI</Text>
      <Text fontSize="sm">
        {' '}
        Slippage tolerance = {uniswapStore.slippageTolerance.toSignificant(2)}%
      </Text>
    </Box>
  )
}

export default observer(TradeInformation)
