import { observer } from 'mobx-react-lite'
import { Box, Button, Center, Flex, FormLabel, Heading, Text, Image } from '@chakra-ui/react'
import { useEffect } from 'react'
import TradeInformation from './TradeInformation'
import NumberInput from '../../components/NumberInput'
import { useRootStore } from '../../context/RootStoreProvider'
import { fromWeiToEther } from '../../utils/web3-utils'

const CoinIcon: React.FC<{ src: string; name: string }> = ({ src, name }) => {
  return (
    <Flex alignItems="center">
      <Image boxSize="6" src={src} alt={name} borderRadius="full" />
      <Text ml="2" fontSize="2xl" fontWeight="700">
        {name}
      </Text>
    </Flex>
  )
}

const InformationText: React.FC = ({ children }) => (
  <Text fontSize="sm" color="gray.500">
    {children}
  </Text>
)

const Swap: React.FC = () => {
  const { web3Store, uniswapStore } = useRootStore()
  const balance = fromWeiToEther(web3Store.balance) ?? '0'
  const swapDisabled =
    uniswapStore.daiInputValue === undefined ||
    uniswapStore.daiInputValue === '' ||
    (uniswapStore.daiInputValue !== undefined && parseFloat(uniswapStore.daiInputValue) <= 0)

  useEffect(() => {
    uniswapStore.init()
  })

  const onClick = (): void => {
    uniswapStore.marketBuyEthWithDai()
  }

  return (
    <Flex w="100%" justifyContent="center" mt="16">
      <Center>
        <Box
          boxShadow="2xl"
          border="1px"
          borderColor="gray.200"
          py="8"
          px={{ base: '4', lg: '16' }}
          borderRadius="16"
        >
          <Flex>
            <Heading as="h1" size="sm" mb="4">
              Swap
            </Heading>
          </Flex>
          <Box>
            <Flex flexDirection="column" mb="4">
              <Flex justifyContent="space-between">
                <FormLabel>
                  <CoinIcon
                    name="DAI"
                    src="https://gemini.com/images/currencies/icons/default/dai.svg"
                  />
                </FormLabel>
                <NumberInput
                  disabled={!web3Store.address}
                  onChange={uniswapStore.onChangeDaiValue}
                  placeholder="0.0"
                  value={uniswapStore.daiInputValue}
                />
              </Flex>
              <InformationText>Balance: {uniswapStore.daiBalance} DAI</InformationText>
            </Flex>
            <Flex flexDirection="column" mb="4">
              <Flex justifyContent="space-between">
                <CoinIcon name="ETH" src="/ETH-icon.png" />
                <NumberInput
                  disabled={!web3Store.address}
                  onChange={uniswapStore.onChangeEthValue}
                  value={uniswapStore.ethInputValue}
                />
              </Flex>
              <InformationText>Balance: {balance} ETH</InformationText>
            </Flex>
          </Box>
          {!swapDisabled && <TradeInformation />}
          <Button onClick={onClick} colorScheme="pink" w="100%" mt="8" disabled={swapDisabled}>
            {swapDisabled ? 'Enter an amount' : 'Swap'}
          </Button>
        </Box>
      </Center>
    </Flex>
  )
}

export default observer(Swap)
