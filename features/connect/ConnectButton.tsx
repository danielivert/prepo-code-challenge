import { observer } from 'mobx-react-lite'
import { Button } from '@chakra-ui/react'
import { useRootStore } from '../../context/RootStoreProvider'
import { getShortAccount } from '../../utils/account-utils'

type Props = { withIcon?: boolean }

const ConnectButton: React.FC<Props> = ({ withIcon = false }) => {
  const { web3Store } = useRootStore()

  const onClickLogin = (): void => {
    web3Store.init(true)
  }

  const onClickLogout = (): void => {
    web3Store.logout()
  }

  const onClick = web3Store.address ? onClickLogout : onClickLogin

  return (
    <Button type="button" onClick={onClick}>
      {withIcon && !web3Store.address && <img alt="Metamask Icon" src="/metamask-icon.png" />}
      {getShortAccount(web3Store.address) ?? 'Connect Wallet'}
    </Button>
  )
}

export default observer(ConnectButton)
