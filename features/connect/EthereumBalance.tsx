import { Center, Flex } from '@chakra-ui/react'
import { observer } from 'mobx-react-lite'
import { useRootStore } from '../../context/RootStoreProvider'
import { fromWeiToEther } from '../../utils/web3-utils'

const EthereumBalance: React.FC = () => {
  const { web3Store } = useRootStore()
  const balance = fromWeiToEther(web3Store.balance) ?? '0'

  return (
    <Flex flexDirection="column" justifyContent="center">
      <Center>{balance} ETH</Center>
    </Flex>
  )
}

export default observer(EthereumBalance)
