import { createContext, useContext } from 'react'
import { RootStore } from '../stores/RootStore'

let store: RootStore

const StoreContext = createContext<RootStore | undefined>(undefined)

export const RootStoreProvider: React.FC = ({ children }) => {
  // only create the store once ( store is a singleton)
  const root = store ?? new RootStore()

  return <StoreContext.Provider value={root}>{children}</StoreContext.Provider>
}

export const useRootStore = (): RootStore => {
  const context = useContext(StoreContext)
  if (context === undefined) {
    throw new Error('useRootStore must be used within RootStoreProvider')
  }

  return context
}
