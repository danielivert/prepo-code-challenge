import { MoonIcon, SunIcon } from '@chakra-ui/icons'
import { Button, useColorMode } from '@chakra-ui/react'

const ToggleTheme: React.FC = () => {
  const { colorMode, toggleColorMode } = useColorMode()
  const isLightThenme = colorMode === 'light'
  const themeText = isLightThenme ? 'Dark Theme' : 'Light Theme'

  return (
    <Button onClick={toggleColorMode}>
      {themeText} {colorMode === 'light' ? <MoonIcon ml="2" /> : <SunIcon ml="2" />}
    </Button>
  )
}

export default ToggleTheme
