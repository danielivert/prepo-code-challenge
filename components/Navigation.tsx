import { Center, Flex } from '@chakra-ui/react'
import { observer } from 'mobx-react-lite'
import ToggleTheme from './ToggleTheme'
import ConnectButton from '../features/connect/ConnectButton'
import EthereumBalance from '../features/connect/EthereumBalance'
import { useRootStore } from '../context/RootStoreProvider'

const Navigation: React.FC = () => {
  const { web3Store } = useRootStore()

  return (
    <Flex>
      <Center w="100%" justifyContent="flex-end" p="8">
        <Flex mr="4">
          <ToggleTheme />
        </Flex>
        <Flex>
          <ConnectButton withIcon />
          {web3Store.address && (
            <Flex ml="4" display={{ base: 'none', sm: 'flex' }}>
              <EthereumBalance />
            </Flex>
          )}
        </Flex>
      </Center>
    </Flex>
  )
}

export default observer(Navigation)
