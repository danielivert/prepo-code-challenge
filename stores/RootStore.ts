import { UniswapStore } from './UniswapStore'
import { Web3Store } from './Web3Store'

export class RootStore {
  web3Store: Web3Store
  uniswapStore: UniswapStore

  constructor() {
    this.web3Store = new Web3Store(this)
    this.uniswapStore = new UniswapStore(this)
  }
}

let rootStore: RootStore

export const initRootStore = (): void => {
  rootStore = new RootStore()
}

export const rs = (): RootStore => rootStore
