import { BigNumber, ethers } from 'ethers'
import { Fetcher, Route, Token, WETH } from '@uniswap/sdk'
import WalletConnectProvider from '@walletconnect/web3-provider'
import Web3 from 'web3'
import { makeObservable, action, observable, when, runInAction, autorun } from 'mobx'
import Web3Modal from 'web3modal'
import { StaticJsonRpcProvider, Web3Provider } from '@ethersproject/providers'
import Swal from 'sweetalert2'
import { RootStore } from './RootStore'
import { DAI_ADDRESS, INFURA_ID, NETWORK, NETWORKS } from '../lib/constants'
import config from '../lib/config'

const isBrowser = Boolean(process.browser)

const mainnetInfura = new StaticJsonRpcProvider(`https://mainnet.infura.io/v3/${INFURA_ID}`)

export class Web3Store {
  root: RootStore
  @observable web3Modal: Web3Modal | undefined = undefined
  @observable provider: Web3Provider | undefined = undefined
  @observable web3Provider: Web3Provider | undefined = undefined
  @observable signer: ethers.Signer | undefined = undefined
  @observable web3: Web3 | undefined = undefined
  @observable address: string | null = null
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @observable mainnetProvider: any = mainnetInfura
  @observable targetNetwork = NETWORKS[config.NETWORK] // 📡 What chain are your contracts deployed to? <------- select your target frontend network (localhost, rinkeby, xdai, mainnet)
  @observable localProviderUrl = this.targetNetwork.rpcUrl // 🏠 Your local provider is usually pointed at your local blockchain
  @observable localProvider = new StaticJsonRpcProvider(this.targetNetwork.rpcUrl)
  @observable balance: ethers.BigNumber = BigNumber.from(0)
  @observable isConnectedToRightNetwork = false
  @observable chainId = 0
  @observable ethPrice = 0

  constructor(root: RootStore) {
    makeObservable(this)
    this.root = root
    this.init()
  }

  @action
  init = async (trigger = false): Promise<void> => {
    await when(() => isBrowser)
    this.reset()

    runInAction(() => {
      const providerOptions = {
        walletconnect: {
          package: WalletConnectProvider,
          options: {
            infuraId: INFURA_ID,
          },
        },
      }
      this.web3Modal = new Web3Modal({
        cacheProvider: true, // optional
        providerOptions, // required
      })

      if (this.web3Modal.cachedProvider || trigger) {
        this.connect()
        this.checkCurrentNetwork()
      }
    })
  }

  @action
  connect = async (): Promise<void> => {
    const provider = await this.web3Modal?.connect()

    runInAction(() => {
      this.provider = provider
    })

    const web3 = new Web3(provider)
    const web3Provider = new ethers.providers.Web3Provider(provider)
    const signer = await web3Provider.getSigner()

    runInAction(() => {
      this.web3 = web3
      this.signer = signer
      this.web3Provider = web3Provider
    })
    await this.subscribeProvider()

    this.setAddress()
    this.getEthPrice()
  }

  handleAccountsChanged = (): void => {
    this.setAddress()
  }

  @action
  setAddress = async (): Promise<void> => {
    if (this.signer) {
      const address = await this.signer.getAddress()
      const balance = await this.signer.getBalance()

      runInAction(() => {
        this.address = address
        this.balance = balance
      })
    }
  }

  @action
  subscribeProvider = async (): Promise<void | null> => {
    this.provider?.on('accountsChanged', this.handleAccountsChanged)

    // Subscribe to chainId change
    this.provider?.on('chainChanged', (chainId: number) => {
      this.init()
    })

    // Subscribe to provider connection
    this.provider?.on('connect', (info: { chainId: number }) => {
      console.log('connected provider', info)
    })

    // Subscribe to provider disconnection
    this.provider?.on('disconnect', (error: { code: number; message: string }) => {
      console.log(error)
    })
  }

  logout = (): void => {
    if (this.web3Modal && this.web3Modal.cachedProvider) {
      this.web3Modal.clearCachedProvider()
    }
    this.reset()
  }

  @action
  checkCurrentNetwork = (): void => {
    autorun(async () => {
      if (this.localProvider && this.provider && this.web3Provider) {
        const localChain = await this.localProvider.getNetwork()
        const localChainId = parseInt(`${localChain.chainId}`, 10)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const selectedChain: any = { ...this.provider }
        const selectedChainId = parseInt(selectedChain.networkVersion, 10)
        const isRightNetwork = localChainId === selectedChainId
        const isWrongNetwork = localChainId && selectedChainId && localChainId !== selectedChainId
        runInAction(() => {
          this.isConnectedToRightNetwork = isRightNetwork
          this.chainId = selectedChainId
        })

        if (isRightNetwork) {
          Swal.close()
        }

        if (isWrongNetwork) {
          const networkSelected = NETWORK(selectedChainId)
          const networkLocal = NETWORK(localChainId)
          const isWrongNetworkId = selectedChainId === 1337 && localChainId === 31337
          if (isWrongNetworkId) {
            Swal.fire({
              toast: true,
              icon: 'error',
              title: '⚠️ Wrong Network ID',
              showConfirmButton: false,
              html: 'You have <strong>chain id 1337</strong> for localhost and you need to change it to <strong>31337</strong> to work with HardHat. <div>(MetaMask -&gt; Settings -&gt; Networks -&gt; Chain ID -&gt; 31337)</div>',
              position: 'bottom-end',
            })
          } else {
            Swal.fire({
              toast: true,
              icon: 'error',
              title: '⚠️ Wrong Network',
              showConfirmButton: false,
              html: `You have <strong>${
                networkSelected && networkSelected.name
              }</strong> selected and you need to be on <strong>${
                networkLocal && networkLocal.name
              }</strong>`,
              position: 'bottom-end',
            })
          }
        }
      }
    })
  }

  @action
  getEthPrice = async (): Promise<void> => {
    const chainId = this.mainnetProvider.network ? this.mainnetProvider.network.chainId : 1
    const DAI = new Token(chainId, DAI_ADDRESS, 18)
    const pair = await Fetcher.fetchPairData(DAI, WETH[DAI.chainId], this.mainnetProvider)
    const route = new Route([pair], WETH[DAI.chainId])

    runInAction(() => {
      this.ethPrice = parseFloat(route.midPrice.toSignificant(6))
    })
  }

  @action
  reset = (): void => {
    this.provider = undefined
    this.web3 = undefined
    this.address = null
    this.ethPrice = 0
    this.balance = BigNumber.from(0)
  }
}
