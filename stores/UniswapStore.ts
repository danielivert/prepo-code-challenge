import { makeObservable, action, observable, runInAction, autorun } from 'mobx'
import { Fetcher, Percent, Route, Token, TokenAmount, Trade, TradeType, WETH } from '@uniswap/sdk'
import { ethers } from 'ethers'
import Swal from 'sweetalert2'
import { RootStore } from './RootStore'
import { Uniswapv2Abi, Uniswapv2Abi__factory } from '../types/ethers-contracts'
import config from '../lib/config'
import {
  DAI_ADDRESS,
  DEFAULT_CHAIN_ID,
  MINIMUM_GAS_FEE,
  ONE_SECOND,
  TWENTY_MINUTES,
} from '../lib/constants'
import { fromWeiToEther } from '../utils/web3-utils'
import { DaiAbi__factory } from '../types/ethers-contracts/factories/DaiAbi__factory'
import { debounce } from '../utils/debounce'

export class UniswapStore {
  root: RootStore
  contract: Uniswapv2Abi | undefined
  @observable daiInputValue: string | undefined
  @observable ethInputValue: string | undefined
  @observable daiBalance = '0'
  @observable tradingPairEth = '0'
  @observable tradingPairDai = '0'
  @observable loading = true
  slippageTolerance = new Percent('50', '10000') // 0.50% slippage

  constructor(root: RootStore) {
    makeObservable(this)
    this.root = root
  }

  @action
  init = async (): Promise<void> => {
    autorun(async () => {
      if (
        this.root.web3Store.isConnectedToRightNetwork &&
        this.root.web3Store.address &&
        this.root.web3Store.signer
      ) {
        const { signer } = this.root.web3Store

        runInAction(() => {
          this.contract = Uniswapv2Abi__factory.connect(config.UNISWAP_V2_CONTRACT_ADDRESS, signer)
        })

        this.getPriceForTradingPair() // Gets the initial value to speed UI first time user writes an amount on any input field
        this.getDaiBalance()
      }
    })
  }

  @action
  getPriceForTradingPair = async (): Promise<void> => {
    const chainId = this.root.web3Store.mainnetProvider.network
      ? this.root.web3Store.mainnetProvider.network.chainId
      : DEFAULT_CHAIN_ID
    const DAI = new Token(chainId, DAI_ADDRESS, 18)

    const pair = await Fetcher.fetchPairData(DAI, WETH[DAI.chainId])
    const route = new Route([pair], WETH[DAI.chainId]) // Route WETH > DAI

    runInAction(() => {
      this.tradingPairEth = route.midPrice.toSignificant(6)
      this.tradingPairDai = route.midPrice.invert().toSignificant(6)
    })
  }

  @action
  getDaiBalance = async (): Promise<void> => {
    if (
      this.root.web3Store.isConnectedToRightNetwork &&
      this.root.web3Store.address &&
      this.root.web3Store.signer
    ) {
      const tokenContract = DaiAbi__factory.connect(DAI_ADDRESS, this.root.web3Store.signer)
      const balance = await tokenContract.balanceOf(this.root.web3Store.address)

      runInAction(() => {
        this.daiBalance = fromWeiToEther(balance)
      })
    }
  }

  marketBuyEthWithDai = async (): Promise<void> => {
    if (
      this.root.web3Store.isConnectedToRightNetwork &&
      this.root.web3Store.address &&
      this.root.web3Store.signer &&
      this.daiInputValue
    ) {
      const chainId = this.root.web3Store.mainnetProvider.network
        ? this.root.web3Store.mainnetProvider.network.chainId
        : DEFAULT_CHAIN_ID
      const DAI = new Token(chainId, DAI_ADDRESS, 18)

      const pair = await Fetcher.fetchPairData(DAI, WETH[DAI.chainId])
      const route = new Route([pair], DAI) // Route DAI > WETH

      const amountIn = ethers.utils.parseEther(this.daiInputValue).toString() // DAI as 1000000000000000000

      const trade = new Trade(route, new TokenAmount(DAI, amountIn), TradeType.EXACT_INPUT)

      const amountOutMin = trade.minimumAmountOut(this.slippageTolerance).raw
      const amountOutMinHex = ethers.BigNumber.from(amountOutMin.toString()).toHexString()
      const path = [DAI.address, WETH[DAI.chainId].address]
      const to = this.root.web3Store.address
      const deadline = Math.floor(Date.now() / 1000) + TWENTY_MINUTES
      const value = trade.inputAmount.raw
      const valueAsHex = ethers.BigNumber.from(value.toString()).toHexString()

      try {
        const tx = await this.contract?.swapExactTokensForETH(
          valueAsHex,
          amountOutMinHex,
          path,
          to,
          deadline,
          {
            gasLimit: MINIMUM_GAS_FEE,
          }
        )
        await tx?.wait()

        Swal.fire({
          toast: true,
          icon: 'success',
          title: 'Transaction Sent',
          showConfirmButton: false,
          timer: ONE_SECOND * 4,
          timerProgressBar: true,
          text: tx?.hash,
          position: 'bottom-end',
        })
      } catch (e: unknown) {
        let errorMessage
        if (typeof e === 'string') {
          errorMessage = e
        } else {
          errorMessage = (e as Error).message
        }
        Swal.fire({
          toast: true,
          icon: 'error',
          title: 'Error Swapping',
          showConfirmButton: false,
          html: errorMessage,
          timer: ONE_SECOND * 3,
          position: 'bottom-end',
        })
      }
    }
  }

  calculateEthValue = debounce(
    (newDaiValue: string, tradingPairDai: string): string | undefined => {
      const daiValueFloat = parseFloat(newDaiValue)
      const tradingPairDaiFloat = parseFloat(tradingPairDai)
      if (!newDaiValue) {
        return ''
      }
      return `${daiValueFloat * tradingPairDaiFloat}`
    },
    350
  )

  calculateDaiValue = debounce(
    (newEthValue: string, tradingPairEth: string): string | undefined => {
      const ethValueFloat = parseFloat(newEthValue)
      const tradingPairEthFloat = parseFloat(tradingPairEth)
      if (!ethValueFloat) {
        return ''
      }

      return `${ethValueFloat * tradingPairEthFloat}`
    },
    350
  )

  @action
  onChangeDaiValue = async (value: string): Promise<void> => {
    const newDaiValue = value
    this.daiInputValue = newDaiValue
    const newEthValue = await this.calculateEthValue(newDaiValue, this.tradingPairDai)
    runInAction(() => {
      this.ethInputValue = newEthValue
    })
  }

  @action
  onChangeEthValue = async (value: string): Promise<void> => {
    const newEthValue = value
    this.ethInputValue = newEthValue
    const newDaiValue = await this.calculateDaiValue(newEthValue, this.tradingPairEth)
    runInAction(() => {
      this.daiInputValue = newDaiValue
    })
  }

  reset = (): void => {
    this.loading = true
  }
}
