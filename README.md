# Prepo-code-challenge

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

See the live demo [here](https://prepo.netrek.se).

## What is this?

This is a Web3 frontend application that displays data for trading ETH/DAI and allows the user to do it.

You can do the following:

- [x] User can connect their MetaMask wallet
- [x] App shows the current Uniswap V2 price for the ETH/DAI trading pair after user input, then it updates on realtime.
- [x] App shows user balance for tokens in the trading pair (DAI & ETH)
- [x] User can submit a transaction (swap) to obtain ETH using a specified amount of DAI.

## How it was built

The app was built with the following stack:

- React using [NextJS](https://nextjs.org/) as framework together with Typescript.
- [MobX](https://mobx.js.org/) for state management.
- [Chakra-UI](https://chakra-ui.com/) for the components.
- [Jest](https://jestjs.io/) for writing and running unitests.
- [Typechain](https://github.com/ethereum-ts/TypeChain), [prettier](https://github.com/prettier/prettier), [eslint](https://eslint.org/), [husky](https://github.com/typicode/husky) and [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) for improving development experience and maintaining code quality.

## Installation

```bash
$ yarn
```

## Running the app

Please have a look at .env.sample to get started

```bash
# development
$ yarn dev

# production mode
$ yarn start
```

## Test

```bash
# unit tests
$ yarn test
```

## Hosting

The application is hosted on [Vercel](https://vercel.com) as static HTML.

## Things I could have done different if I had more time

- Make generic fields for the tokens instead of hard coding ETH/DAI.
- Properly test the swap functionality by trying to get test tokens in ropsten or any other ethereum test network. Add proper loading states after transaction is being done and completed.
- Explore a more precise way to calculate the cost of the swap by optaining the liquity provider fee.
- Render the route that was taken for the swap to the user.
- Write tests on the stores methods that are relevant.
- Work a bit more on performance/optimization to improve page loading time. Example: Using skeleton components, lazy loading images, etc.
- Explore fetching all data using graphql subscriptions and [Uniswap Graph](https://thegraph.com/explorer/subgraph?id=0x9bde7bf4d5b13ef94373ced7c8ee0be59735a298-2&view=Playground).
- Try hosting the site on [fleek](https://fleek.co).
